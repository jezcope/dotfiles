{ config, pkgs, lib, ... }:

with builtins;
let
  util = import ./util.nix { inherit pkgs; };
  hostname = lib.strings.fileContents /etc/hostname;
  host-config = ./machines + "/${hostname}.nix";
in
{
  programs.home-manager.enable = true;

  imports = [
    ./bits/desktop.nix
    ./bits/shell.nix
    ./bits/emacs.nix
    ./bits/vim.nix
  ] ++ lib.lists.optional (pathExists host-config) host-config;

  home.username = "jez";
  home.homeDirectory = "/home/jez";

  home.stateVersion = "20.09";

  home.packages = with pkgs;
    let
      py = pkgs.python38Packages;
    in [
      nix

      nmap avahi iftop s3cmd dnsutils whois

      yubikey-manager yubioath-desktop yubikey-personalization yubikey-personalization-gui
      gopass pass protonmail-bridge tor torsocks
      transmission-gtk unison

      aspell aspellDicts.en aspellDicts.en-computers

      bpytop ranger vifm zeal
      gomuks

      graphicsmagick qrencode

      mpc_cli

      python3 pipenv pew py.ipython py.python-language-server

      pandoc texinfo graphviz zotero

      R rstudio gcc_latest

      ## Actually, don't install these:
      ## Use direnv or nix-shell instead
      # rustup clang go gfortran
      # nodejs yarn
      # ghc stack cabal-install hlint
      # h5utils

      virt-manager virt-viewer vagrant
    ];

  home.sessionVariables = {
    WORKON_HOME = ~/.local/lib/languages/Python/virtualenvs;
    RUSTUP_HOME = ~/.local/lib/languages/Rust/rustup;
    CABAL_CONFIG = ~/.config/cabal/config;
    CABAL_DIR = ~/.local/lib/languages/Haskell/cabal;

    PASSWORD_STORE_DIR = ~/Reference/Passwords;
    PASSWORD_STORE_KEY = "9E42CE071C4559D1";

    EDITOR = pkgs.vim + /bin/vim;

    TERMINFO_DIRS = lib.concatStringsSep ":" [
      "~/.nix-profile/share/terminfo"
      ""
    ];
  };

  home.file = {
    ".xprofile".source = ./dotfiles/xprofile;

    ".ackrc".source = ./dotfiles/ackrc;
    ".xmonad".source = ./dotfiles/xmonad;
    ".tmux.conf".source = ./dotfiles/tmux.conf;
    ".Rprofile".source = ./dotfiles/Rprofile;

    ".config/ranger" = {
      source = ./dotfiles/ranger;
      recursive = true;
    };

    "texmf" = {
      source = ./dotfiles/texmf;
      recursive = true;
    };
    "bin/shared".source = ./dotfiles/sharedbin;

    ".config/cabal" = {
      source = ./dotfiles/cabal;
      recursive = true;
    };

    ".terminfo/x/xterm-kitty" = {
      source = pkgs.kitty + /lib/kitty/terminfo/x/xterm-kitty;
      recursive = true;
    };
  };

  programs.man.generateCaches = true;

  programs.git = {
    enable = true;
    aliases = {
      st = "status";
      ci = "commit";
      co = "checkout";
      h = "help";
      a = "add";
      d = "diff";
      b = "branch";
      m = "merge";
      l = "log";
    };
    includes = [
      { path = "~/.config/git/local"; }
    ];
    ignores = [
      "*.pyc"
      "*.log" "*.aux" "*.lol" "*.out" "*.toc" "*.bcf" "*.bbl" "*.blg" "*.nav" "*.run.xml" "*.snm" "*.fdb_latexmk"
      "*.fls" "*.vrb"
      "doc/tags" ".sass-cache" ".sconsign.dblite"
      ".*.sw[op]" ".DS_Store" "._*" "~$*" ".*.un~" "*~" "\\#*" ".\\#*" "*.bak" ".~lock.*\\#"
      ".ipynb_checkpoints" ".stfolder" ".vscode"
      ".envrc"
    ];
    extraConfig = {
      color.ui = "auto";
      push.default = "simple";
      pull.rebase = "false";
      github.user = "jezcope";
      fetch.recurseSubmodules = "true";
      init.defaultBranch = "main";
      pager.branch = "false";
    };
  };

  programs.gpg = {
    enable = true;
    settings = {
      default-key = "0x9E42CE071C4559D1";
      keyserver = "hkps://hkps.pool.sks-keyservers.net";
      keyserver-options = "no-honor-keyserver-url include-revoked";
    };
  };

  services.gpg-agent = {
    enable = true;
    enableScDaemon = true;
    enableSshSupport = true;
    defaultCacheTtl = 3600;
    extraConfig = ''
      no-allow-external-cache
    '';
  };

  services.keybase.enable = true;
  services.kbfs = {
    enable = true;
    mountPoint = "Keybase";
  };

  programs.texlive = {
    enable = true;
    extraPackages = tpkgs: { inherit (tpkgs) scheme-medium wrapfig capt-of multirow; };
  };

}

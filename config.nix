{
  allowUnfree = true;

  packageOverrides = pkgs:
    let
      util = import ./util.nix { inherit pkgs; };
    in
      {
        kitty = util.nixGLWrapMaybe pkgs.kitty "kitty";
      };

  joypixels.acceptLicense = true;
}

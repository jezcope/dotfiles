{ config, pkgs, lib, ... }:

{
  home.file = {
    ".zsh" = {
      source = ../dotfiles/zsh;
      recursive = true;
    };
    ".zsh/omz-custom/plugins/zsh-syntax-highlighting" = {
      source = fetchGit {
        url = "https://github.com/zsh-users/zsh-syntax-highlighting.git";
      };
    };
  };

  home.packages = with pkgs; [
    ripgrep fd fasd unzip rsync pv unison file tree

    curl jq

    shellcheck rmlint progress
    colordiff screenfetch termdown
  ];

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    enableAutosuggestions = true;

    shellAliases = {
      sc = "systemctl";
      scu = "systemctl --user";

      hm = "home-manager";
      hmsw = "home-manager switch";
      hmbld = "home-manager build";
      hmnews = "home-manager news";
      nixupd = "nix-channel --update";
      nixupg = "nix-env --upgrade --leq";
      nixq = "noglob nix-env --query --available";

      e = "emacsclient -t";   # Terminal
      ew = "emacsclient -c";  # New window (and wait)
      en = "emacsclient -nc"; # New window (and return immediately)
    };

    envExtra = ''
      [[ -r /etc/profile.d/nix.sh ]] && source /etc/profile.d/nix.sh
      [[ -r $HOME/.nix-profile/etc/profile.d/nix.sh ]] && source $HOME/.nix-profile/etc/profile.d/nix.sh

      export LOCALE_ARCHIVE="$(nix-build --no-out-link "<nixpkgs>" -A glibcLocales)/lib/locale/locale-archive"
      ZSH_CUSTOM=$HOME/.zsh/omz-custom

      unset LIBGL_DRIVERS_PATH LD_LIBRARY_PATH
    '';

    oh-my-zsh = {
      enable = true;
      plugins = [
        "vi-mode"
        "git" "git-flow"
        "fd" "fasd"
        "virtualenv" "rvm"
        "extract"
        "vagrant" "docker"
        "archlinux"
        "kitty"
        "zsh-syntax-highlighting"
      ];
    };
  };

  programs.fzf.enable = true;
  programs.lesspipe.enable = true;

  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      time.disabled = false;
      hostname.ssh_only = false;
      format = builtins.concatStringsSep "" [
        "$username" "$hostname"
        "$kubernetes" "$directory"
        "$git_branch" "$git_commit" "$git_state" "$git_status"
        "$hg_branch"
        "$docker_context"
        "$package"
        "$dotnet" "$elixir" "$elm" "$erlang" "$golang" "$java" "$julia" "$nim"
        "$nodejs" "$ocaml" "$php" "$purescript" "$python" "$ruby" "$rust" "$terraform"
        "$zig"
        "$nix_shell" "$conda"
        "$memory_usage"
        # "$aws"
        "$env_var" "$crystal" "$cmd_duration"
        "$custom" "$time"
        "$line_break"
        "$jobs" "$battery" "$character"
      ];
    };
  };

  programs.direnv = {
    enable = true;
    enableNixDirenvIntegration = true;
  };

  services.lorri.enable = true;

}

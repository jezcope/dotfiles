{ config, pkgs, lib, ... }:

{
  home.packages = with pkgs; [
    editorconfig-core-c
  ];

  home.file = let
    spacemacs = builtins.fetchGit {
      url = "https://github.com/syl20bnr/spacemacs";
      ref = "develop";
    };
  in {
    # TODO: work out how to do this properly
    # ".emacs.d" = {
    #   source = spacemacs;
    #   recursive = true;
    # };
    ".spacemacs".source = ../dotfiles/spacemacs;
    ".spacemacs.d/private" = {
      source = ../dotfiles/spacemacs-private;
      recursive = true;
    };

    ".local/share/applications/org-protocol.desktop".source = let
      org-protocol-desktop = pkgs.makeDesktopItem {
        name = "org-protocol";
        desktopName = "Emacs org-protocol handler";
        exec = "${pkgs.emacs}/bin/emacsclient %u";
        terminal = "false";
        categories = "System;";
        mimeType = "x-scheme-handler/org-protocol;";
      };
    in org-protocol-desktop + /share/applications/org-protocol.desktop;
  };

  programs.emacs.enable = true;

  services.gpg-agent.extraConfig = ''
      allow-emacs-pinentry
  '';
}

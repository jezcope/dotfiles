{ config, pkgs, lib, ... }:

let
  util = import ../util.nix { inherit pkgs; };
in
{
  home.packages = with pkgs;
    let
      gn = pkgs.gnome3;
    in [
      wofi dmenu swayidle grim slurp light wl-clipboard

      gn.gnome-tweaks gmrun
      mate.atril mate.caja libreoffice-fresh abiword
      inkscape gimp

      openshot-qt blender obs-studio gn.cheese vlc v4l-utils
      pamixer playerctl pavucontrol vorbis-tools audacity
      pulseaudio jack2 cadence non

      atom element-desktop slack zoom-us teams tdesktop

      # Fonts
      iosevka fira fira-code merriweather gentium gentium-book-basic open-sans
      joypixels noto-fonts-emoji font-awesome
    ];

  home.file = {
    ".config/i3".source = ../dotfiles/i3;
    ".config/alacritty".source = ../dotfiles/alacritty;
    ".config/sway".source = ../dotfiles/sway;
    ".config/wofi".source = ../dotfiles/wofi;
    ".config/waybar".source = ../dotfiles/waybar;
    ".config/kitty" = {
      source = ../dotfiles/kitty;
      recursive = true;
    };

    ".face".source = ../dotfiles/images/cat.jpg;
  };

  home.activation = {
    updateFcCache = lib.hm.dag.entryAfter ["writeBoundary"] ''
      $DRY_RUN_CMD fc-cache -f
    '';
  };

  gtk = {
    enable = true;

    theme.name = "Equilux";
    theme.package = pkgs.equilux-theme;
    font.name = "Fira Sans Condensed Regular 10";
    font.package = pkgs.fira;
    iconTheme.name = "Flat-Remix-Green-Dark";
    iconTheme.package = pkgs.flat-remix-icon-theme;

    gtk3.extraConfig = {
      gtk-xft-antialias = 1;
      gtk-xft-hinting = 1;
      gtk-xft-hintstyle = "hintmedium";
      gtk-xft-rgba = "rgb";
    };
  };

  programs.mako = {
    enable = true;
    defaultTimeout = 10000;
  };

  programs.waybar = {
    enable = true;
    systemd.enable = true;
  };

  services.gammastep = {
    enable = true;
    provider = "geoclue2";
    tray = true;
  };

  programs.kitty = {
    enable = true;
    font.name = "Iosevka Medium";
    font.package = pkgs.iosevka;
    settings = {
      font_size = "11.0";

      allow_remote_control  = true;
      listen_on = "unix:\${XDG_RUNTIME_DIR}/kitty.sock";

      enable_audio_bell = false;
      visual_bell_duration = "0.1";
    };

    extraConfig = ''
      # Base16 Gruvbox dark, hard - kitty color config
      # Scheme by Dawid Kurek (dawikur@gmail.com), morhetz (https://github.com/morhetz/gruvbox)
      background #1d2021
      foreground #d5c4a1
      selection_background #d5c4a1
      selection_foreground #1d2021
      url_color #bdae93
      cursor #d5c4a1
      active_border_color #665c54
      inactive_border_color #3c3836
      active_tab_background #1d2021
      active_tab_foreground #d5c4a1
      inactive_tab_background #3c3836
      inactive_tab_foreground #bdae93
      tab_bar_background #3c3836

      # normal
      color0 #1d2021
      color1 #fb4934
      color2 #b8bb26
      color3 #fabd2f
      color4 #83a598
      color5 #d3869b
      color6 #8ec07c
      color7 #d5c4a1

      # bright
      color8 #665c54
      color9 #fb4934
      color10 #b8bb26
      color11 #fabd2f
      color12 #83a598
      color13 #d3869b
      color14 #8ec07c
      color15 #fbf1c7

      # extended base16 colors
      color16 #fe8019
      color17 #d65d0e
      color18 #3c3836
      color19 #504945
      color20 #bdae93
      color21 #ebdbb2
    '';
  };

  programs.firefox = {
    enable = true;
    enableGnomeExtensions = true;
  };
  programs.chromium.enable = true;

  programs.ncmpcpp.settings = {
    user_interface = "alternative";
  };

  services.syncthing.enable = true;
}

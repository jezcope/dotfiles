{ config, pkgs, lib, ... }:

{
  programs.vim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      julia-vim vim-toml
    ];
    settings = {
      expandtab = true;
      tabstop = 2;
      shiftwidth = 2;
    };
  };
}

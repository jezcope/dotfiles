{ config, pkgs, lib, ... }:

let
  util = import ../util.nix { inherit pkgs; };
  settingsFormat = pkgs.formats.json { };
  hostname = lib.strings.fileContents /etc/hostname;
in {
  home.packages = [ pkgs.resilio-sync ];
  home.file.".cache/rslsync/debug.txt".text = ''
    80000000
    0
  '';

  systemd.user.services = let
    confFile = settingsFormat.generate "rslsync.conf" {
      device_name = util.capitalise hostname;
      storage_path = toString ~/.cache/rslsync;
      pid_file = toString ~/.cache/rslsync/rslsync.pid;

      use_upnp = true;

      download_limit = 0;
      upload_limit = 0;

      directory_root = toString ~/.;

      webui.listen = "0.0.0.0:8888";
    };
  in {
    rslsync = {
      Unit = {
        Description = "Resilio Sync per-user service";
        After = "network.target";
      };
      Service = {
        Type = "simple";
        ExecStart =
          "${pkgs.resilio-sync}/bin/rslsync --nodaemon --config ${confFile}";
        Restart = "on-abort";
      };
      Install = { WantedBy = [ "default.target" ]; };
    };
  };
}

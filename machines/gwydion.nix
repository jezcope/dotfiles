{ pkgs, ... }:

{
  home.packages = with pkgs; [
    steam
  ];

  services.mpd = {
    enable = true;
    musicDirectory = ~/Music/Best;
    extraConfig = ''
      audio_output {
        type "pulse"
        name "Shared output (pulseaudio)"
      }

      audio_output {
        type "jack"
        name "Jack output"
      }
    '';
  };
  programs.ncmpcpp.enable = true;
  services.mpdris2.enable = true;
}

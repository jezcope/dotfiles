{ pkgs, lib, ... }:

with builtins;
let
  nixGL = pkgs.callPackage (fetchTarball https://github.com/guibou/nixGL/archive/master.tar.gz) { inherit pkgs; };
in
{

  home.packages = with nixGL; [ (nixGLCommon nixGLIntel) nixGLIntel nixVulkanIntel ];

  home.file = {
    ".pam_environment".text = ''
      XDG_DATA_DIRS=${toString ~/.nix-profile/share}:/usr/local/share:/usr/share
    '';

    ".config/fontconfig/conf.d/10-nix-fonts.conf".text = ''
      <?xml version="1.0"?>
      <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
      <fontconfig>
        <dir>~/.nix-profile/share/fonts</dir>
      </fontconfig>
    '';
  };

  services.mpd = {
    enable = true;
    musicDirectory = ~/Music/Best;
    extraConfig = ''
      audio_output {
        type "pulse"
        name "Shared output (pulseaudio)"
      }

      audio_output {
        type "jack"
        name "Jack output"
      }
    '';
  };
  programs.ncmpcpp.enable = true;
  services.mpdris2.enable = true;

}

{ pkgs, ... }:

let
  lib = pkgs.lib;
  nixGL = pkgs.callPackage (fetchTarball https://github.com/guibou/nixGL/archive/master.tar.gz) { inherit pkgs; };
in
{
  inherit nixGL;

  nixGLWrapMaybe = let
    makeGLWrapper = pkg: cmd: with nixGL; pkgs.writeScriptBin cmd ''
      #!${pkgs.stdenv.shell}
      exec ${nixGLCommon nixGLIntel}/bin/nixGL ${pkg}/bin/${cmd} "$@"
    '';
    nixGLWrap = pkg: cmd: let
      wrapped = makeGLWrapper pkg cmd;
    in pkgs.symlinkJoin { name = "nixGL-${pkg.name}"; paths = [ wrapped pkg ]; };
    isNixOS = builtins.pathExists /etc/NIXOS;
  in pkg: cmd: if isNixOS then pkg else (nixGLWrap pkg cmd);

  capitalise = str:
    let chars = lib.stringToCharacters str;
    in lib.concatStrings ([(lib.toUpper (lib.head chars))] ++ (lib.tail chars));
}

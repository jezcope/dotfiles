FROM archlinux

RUN pacman -Sy --noconfirm zsh git
RUN useradd -u 1000 -s /usr/bin/zsh -m jez
RUN install -d -m755 -o 1000 -g 1000 /nix

USER jez
ENV USER jez
WORKDIR /home/jez
RUN curl -L https://nixos.org/nix/install | sh

RUN . /home/jez/.nix-profile/etc/profile.d/nix.sh \
  && nix-channel --add https://github.com/rycee/home-manager/archive/master.tar.gz home-manager \
  && nix-channel --update \
  && nix-shell '<home-manager>' -A install

COPY --chown=jez:jez config .config/nixpkgs/config
COPY --chown=jez:jez home.nix config.nix .config/nixpkgs/
RUN ls -l /home/jez/.config/nixpkgs
RUN . /home/jez/.nix-profile/etc/profile.d/nix.sh \
  && home-manager --show-trace switch

CMD /usr/bin/zsh --interactive
